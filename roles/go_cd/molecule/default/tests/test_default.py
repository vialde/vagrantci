import os
import pytest


def test_server_is_installed(host):
    goserver = host.package("go-server")

def test_agent_is_installed(host):
    goagent = host.package("go-agent")

def test_server_running_and_enabled(host):
    goserver = host.service("go-server")
    assert goserver.is_running
    assert goserver.is_enabled

def test_agent_running_and_enabled(host):
    goagent = host.service("go-agent")
    assert goagent.is_running
    assert goagent.is_enabled

def test_server_url(host):
    url = host.addr("localhost")
    url.port(8153).is_reachable