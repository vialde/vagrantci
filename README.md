# README #

Using a configuration management language of your choosing (Ansible, Puppet, Salt, Chef) setup a Continuous Integration server of your choosing on a Vagrant Box. You are not required to setup Vagrant or its requirements on the host machine.

### Discussion ###

I decided to try and install a CI server that I've never used before, There's only so many times I can install Gitlab and Jenkins.  I decided on Go-CD, mainly because there seemed to see a lot of buzz around about it, yet I'd never heard of it.  There wasn't a ton of support around so that meant I couldn't really fall back onto ansible galaxy or pre-built vagrant boxes.  There were definitely some limitations as far as automated installation goes, especially around users and security, but it might just be a matter of missing docs.  I managed to combine the server and a basic agent install into one ansible role.

### Prerequisites ###

* Vagrant
* Virtual Box
* Ansible
* Molecule (opional)


### Suggested Setup ###

A standard Vagrant install should work without any issues if you just want to use the ansible checks and assertions for testing.  

If you want to run Molecule tests it is advised to run the molecule commands from virtualenv 
```
python3 -m venv molecule
source molecule/bin/activate
pip install ansible molecule molecule-vagrant
cd <role directory>
molecule (test|create|converge|verify|destroy)
```

### Usage ###

```
vagrant up
``` 

should bring up an Ubuntu instance in virtualbox followed by an ansible run to deploy the server and agent using playbook.yml and roles/go_cd.  Ansible should alert if there are any errors

```
cd roles/go_cd
molecule test
```

Will initiate a molecule run which will do much the same as `vagrant up` but with a little more configurability and better tests using testinfra

### Testing ###

Application should be avilable locally on http://localhost:8153/go/

Tests are accomplished via assertions within the ansible role itself and consists of 3 main tests
* test for server start
* test for agent start
* assert that the url is accessible and has content.

Molecule files have been provided for completeness and provide tests for
* package present go-server and go-agent
* test for server start and enable
* test for agent start and enable
* assert that the url is accessible and has content.

### TODO ###
* agents need to be configured manually at the moment...auto registration possible, see https://docs.gocd.org/current/advanced_usage/agent_auto_register.html
* implement full molecule testing to a target env (aws, azure etc)
* Securiy needs to be tightened up, admin passwords etc
* Role and test needs parameterizing to make more flexible, (hostname, ip, etc)
* versions should be specified where appropriate to ensure that issues aren't introduced by upstream changes