Role Name
=========

This is a role to install GoCD server and agent

Requirements
------------

Role needs elevated privleges

Role Variables
--------------

No configurable variables

Dependencies
------------

None

Example Playbook
----------------

---
  - name: Install GO CD Server
    hosts: all
    become: yes

    roles:
      - go_cd

License
-------

BSD

Author Information
------------------

Cary Corse
